"use strict"
const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();
const msp = require("msgpack5")();

var AnalogNode = require("./analog");

module.exports = function (RED) {

        class TankNode extends AnalogNode {
                constructor(config) {
                        super(RED, config);
                        this.fapiEndpoint = "/api/services/tank/state",
                        this.height = parseFloat(config.height);
                }

                // Base method for processing the value received in senML.
                calculateValue(value) {
                        return (1.0 / this.height) * parseFloat(value);
                }
        }

        RED.nodes.registerType("tank", TankNode);

}

