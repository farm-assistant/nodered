"use strict"
const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();
const msp = require("msgpack5")();

var AnalogNode = require("./analog");

module.exports = function (RED) {

    class LoadCellNode extends AnalogNode {
        constructor(config) {
	    config.port = "LOADC";
            super(RED, config);
            this.fapiEndpoint = "/api/services/loadcell/state";
        }

        processValue(value) {
                        return Number(value.toFixed(0));
        }
    }

    RED.nodes.registerType("load-cell", LoadCellNode);
}


