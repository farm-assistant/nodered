const xst = require('fa-gateway-lib');
const config = require('farm-assistant-shim/config');
const logger = require('farm-assistant-shim/config').logger;
const events = require("../../red/runtime/events");
const socketsPath = config.gateway.socketsPath;
const faConfig    = config;

module.exports = function(RED) {
    "use strict";
    var util = require("util");
    var isUtf8 = require('is-utf8');
    var EventEmitter = require("events");
    var net = require("net");
    var fs = require("fs");
    var path = require("path");

    var Events = EventEmitter.EventEmitter;

    // "Private" Socket object
    class IPCSocket extends EventEmitter {

        constructor(name) {
            super();
            var $this = this;
            this.name = name;
            this.socketPath = IPCSocket._formatSocketPath(name);
            this.connected = false;
            this.sock = null;
            this._busy = false;
            
            this.reset  =function(){
                console.log("IPC Socket","reset");
                $this.sock.emit("end");
            }

            this.setBusy = function(state = true) {
                $this._busy = state;
                console.log("IPC Socket", "busy", state);
                $this.emit('busy', state);
            }


            this.receivingTimeout = 500;
            // Buffer to hold multi-packet incoming transmissions
            this.receivingBuffer = Buffer(0);

            setInterval(() => {
                if (!this.connected) this.emitData();
            }, 2000);
        }

        // Return a fresh socket
        _createSocket() {
            return net.createConnection({
                path: this.socketPath,
                timeout: this.receivingTimeout
            });
        }

        // Create the absolute filename for the unix socket 
        static _formatSocketPath(name) {
            return path.format({
                dir: socketsPath,
                base: name,
                ext: ''
            });
        }

        // Write data to the socket
        write(data) {
            console.log("Writing ", data);
            if (this.connected) {
                this.sock.write(data);
            }
        }


        // Callback when socket has connected, callback reject() on error.
        connect(success, reject) {
            var sock = this._createSocket();
            sock.on('connect', () => {
                success(sock);
            });
            sock.on('error', (error) => {
                reject(error);
            });
        }

        // Emit data received from IPC Socket, reconnects on error
        emitData() {
            // Store reference to object instance
            var self = this;
            if (!_socketExists()) {
                return _handleDisconnect();
            }
            this.connect((sock) => {
                console.log("IPC Node", "Connect");
                self.sock = sock;
                self.sock.on('data', _processData);
                self.sock.on('error', _handleDisconnect);
                self.sock.on('timeout', _flushData);
                self.sock.on('end', _handleDisconnect);
		self.sock.setTimeout(500);
                _setConnected();
                //self.sock.on('data', (data) =>_processData(data));
                //self.sock.on('error', (error) => _handleDisconnect(error));
                //self.sock.on('timeout', () => _flushData());
                // self.sock.on('end', () => _handleDisconnect());
            }, (error) => {
                _handleDisconnect(error);
            });

            function _socketExists() {
                try {
                    return fs.existsSync(self.socketPath);
                } catch (e) {
                    return false;
                }
            }

            function _setConnected() {
                self.emit('connected');
                self.connected = true;
            }

            function _setDisconnected() {
                self.emit('disconnected');
                self.connected = false;
            }

            // Meat and Potatos of the whole class


            function _processData(data) {
               	console.log("IPC","Got data");
                self.receivingBuffer = Buffer.concat([self.receivingBuffer, data]);
            }

            function _flushData() {
                if (self.receivingBuffer.length) {
                    console.log("IPC Flushing", self.receivingBuffer);
                    self.emit('data', self.receivingBuffer);
                    self.receivingBuffer = Buffer(0);
                }
            }

            // Handle errors


            function _handleDisconnect(error = null) {
                console.log("IPC Node", "DisConnect");
                // If error is null, treat disconnect as friendly   
                if (error) {
                    console.error(error);
                }
                if (self.sock != null) {
                    console.log("IPC Socket - handle disconnect");
                    self.sock.removeListener('data', _processData);
                    self.sock.removeListener('error', _handleDisconnect);
                    self.sock.removeListener('timeout', _flushData);
                    self.sock.removeListener('end', _handleDisconnect);
                    self.sock.destroy();
                }
                _setDisconnected();
            }
        }
    }

    class IPCSocketSingleton {

        constructor() {
            if (!Sockets.hasOwnProperty('agsense')) {
                Sockets.agsense = {};
                Sockets.agsense.sockets = {};
            }
        }


        getInstance(socketName) {

            console.log("IPCSocketSingleton", socketName);

            if (Sockets.agsense.sockets.hasOwnProperty(socketName)) {
                return Sockets.agsense.sockets[socketName];
            } else {
                var new_sock = new IPCSocket(socketName);
                Sockets.agsense.sockets[socketName] = new_sock;
                return new_sock;
            }
        }
    }

    function IPCSocketNode(n) {
        RED.nodes.createNode(this, n);
        console.log(n);
        Events.call(this);
        var node = this;
        node.socket = n.socket;
        var owners = [];

        // Configuration options passed by Node Red
        var _data = function(data) {
            owners.forEach(function(owner) {
                owner.emit('data', data);
            });
        };

        var _error = function(error) {
            console.log("Handling error!", error);
        };

        var _busy = function(state) {
            owners.forEach(function(owner) {
                owner.emit('busy', state);
            });
        };

        var _connected = function() {
            owners.forEach(function(owner) {
                owner.emit('connected');
            });
        };

        var _disconnected = function() {
            owners.forEach(function(owner) {
                owner.emit('disconnected');
            });
        };

        console.log("this.socket is ", node.socket);


        node.connect = function(owner) {
            console.log("Starting IPC", this.id, owner.friendly_name);
            node.socketSingleton = new IPCSocketSingleton().getInstance(node.socket);
            node.name = node.socketSingleton.name;
            node.write = node.socketSingleton.write.bind(node.socketSingleton);
            node.reset = node.socketSingleton.reset.bind(node.socketSingleton);

            node.setBusy = node.socketSingleton.setBusy;

            if (owners.length == 0) {
		console.log("IPC Setting up event handlers!");
                node.socketSingleton.on('busy', _busy);
                node.socketSingleton.on('data', _data);
                node.socketSingleton.on('error', _error);
                node.socketSingleton.on('connected', _connected);
                node.socketSingleton.on('disconnected', _disconnected);
            }
            owners.push(owner);
        }
    }

    //IPCSocket.prototype = Object.create(Events.prototype);
    //IPCSocketNode.prototype = Object.create(Events.prototype);
    RED.nodes.registerType("ipc-socket", IPCSocketNode);

    function IPCSocketInNode(n) {
        RED.nodes.createNode(this, n);
        node.socket = n.socket;
        node.socketConn = RED.nodes.getNode(node.socket);

        if (node.socketConn) {

            node.socketConn.on('disconnected', function() {
                node.status({
                    fill: "red",
                    shape: "ring",
                    text: "disconnected"
                });
            });

            node.socketConn.on('connected', function() {
                node.status({
                    fill: "green",
                    shape: "dot",
                    text: "connected"
                });
            });

            node.socketConn.on('data', function(msg) {
                // msg.topic contains name of device
                console.log("Message received by IPC Socket In:", msg);
                var _msg = {};
                _msg.topic = "device/" + node.socketConn.name + "/update";
                _msg.payload = msg.payload;
                node.send(_msg);
            });
        }
    }


    RED.nodes.registerType("socket in", IPCSocketInNode);

    function IPCSocketOutNode(n) {
        RED.nodes.createNode(this, n);
        this.socket = n.socket;
        this.socketConn = RED.nodes.getNode(this.socket);
        var node = this;

        if (this.socketConn != null) {

            this.socketConn.on('disconnected', function() {
                node.status({
                    fill: "red",
                    shape: "ring",
                    text: "disconnected"
                });
            });

            this.socketConn.on('connected', function() {
                node.status({
                    fill: "green",
                    shape: "dot",
                    text: "connected"
                });
            });
        }

        this.on("input", function(msg) {

            console.log("On input");

            if (!node.socketConn || this.socketConn.status == false) return;

            console.log("Writing to socket:", msg.payload);
            node.socketConn.write(msg.payload);
        });
    }

    RED.nodes.registerType("socket out", IPCSocketOutNode);
    RED.httpAdmin.get("/ipc/sockets/available", function(req, res) {
        var paths = {};
        var _id = req.query.id || "";
        var _path = socketsPath;
        var _paths = fs.readdirSync(_path);
        var _links = {};

        var _exclude = [];

        RED.nodes.eachNode(function(node) {
            if (node.type === 'ipc-socket' && node.id != _id) { // ensure sockets are unique
                _exclude.push(node.socket);
            }
        });

        console.log("Checking paths");

        for (var idx in _paths) {
            var stat = fs.lstatSync(_path + "/" + _paths[idx]);
            var stat2 = fs.statSync(_path + "/" + _paths[idx]);
            if (stat.isSymbolicLink()) {
                _links[stat2.ino] = _paths[idx];
            }
        }
        for (var idx in _paths) {
            var stat = fs.lstatSync(_path + "/" + _paths[idx]);
            if ((stat.isSocket() & !stat.isSymbolicLink())) {
                if (_exclude.indexOf(_paths[idx]) != -1) // omit excluded
                continue;

                paths[_paths[idx]] = _links.hasOwnProperty(stat.ino) ? _links[stat.ino] : _paths[idx];
            }
        }
        res.send(JSON.stringify(paths))
    });
    // Provide an updater for the client side
    RED.httpAdmin.get("/device/*/request", function(req, res) {
        var _id = req.query.id || "";

        try {


            console.log("Emit request", _id);

            var _node = RED.nodes.getNode(_id);
            
            if (_node)
                _node.emit('request');
        } catch (e) {
            console.log(e);
            res.send({
                status: "error"
            });
            return;
        }
        res.send({
            status: "updated"
        });
    });

   	RED.httpAdmin.get('/leaflet.css', function(req, res){
        var filename = path.join(__dirname , 'leaflet-locationpicker', 'leaflet.css');
        res.sendFile(filename);
    });
  

   	RED.httpAdmin.get('/locationpicker.js', function(req, res){
        var filename = path.join(__dirname , 'leaflet-locationpicker', 'leaflet.js');
        res.sendFile(filename);
    });
  
    RED.httpAdmin.get("/config", function(req, res) {
        try {
            var key =  req.query.key;
            var val;
            if (faConfig.hasOwnProperty(key))
                val =  faConfig[key]
            else
                val = faConfig.got(key)
            
            return res.status(200).json({
                key:key,
                val:val
            });
        } catch (e) {
            return res.status(500).json({
                status: "ERROR " + e.toString()
            });
        }
    });

}
