
var NodeRedNode = require("./NodeRedBase.js");

module.exports = function (RED) {

		class VideoStreamNode extends NodeRedNode {
				constructor(config) {
						super(RED, config);
						this.friendly_name = config.friendly_name;
				
						this.feedUrl = config.url;
						if (config.lat && config.lon) {
						    this.location = [config.lat, config.lon];
						}

						this.fapiEndpoint = "/SOMETHING"; // TODO

						this.on("request", () => {
						});
				}
		}
    RED.nodes.registerNode("videostream", VideoStreamNode);
}

