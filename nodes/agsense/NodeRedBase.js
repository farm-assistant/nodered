

class NodeRedNode {
		constructor(RED, config) {
				RED.nodes.createNode(this, config);
		}

		// Show "connected" status in NodeRED
		setConnected() {
				var connectedStatus = {
						fill: "green",
						shape: "dot",
						text: "connected"
				};

				this.status(connectedStatus);
		}

		// Show "updating" status in NodeRED
		setUpdating() {
				var updatingStatus = {
						fill: "yellow",
						shape: "dot",
						text: "updating"
				};

				this.status(updatingStatus);
		}

		// Show "disconnected" status in NodeRED
		setDisconnected() {
				var disconnectedStatus = {
						fill: "red", 
						shape: "ring",
						text: "disconnected"
				};

				this.status(disconnectedStatus);
		}
}

module.exports = NodeRedNode;

