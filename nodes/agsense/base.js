"use strict"
const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();
const msp = require("msgpack5")();
const level = require('level');

const MAX_QUEUED_ATTEMPTS = 10; // Repeat queue attempts 
const MIN_WAIT_UPDATING   = 20000; // ms from  update => connected

var ipc = require("./ipc-socket.js");
var NodeRedNode = require("./NodeRedBase.js");

// Base class for all Agsense Nodes
class AgsenseNode extends NodeRedNode {
    constructor(RED, config) {
        super(RED, config);

        var $this = this;
        this.friendly_name = config.friendly_name;
        
        // IPC Socket Wrapper
        this.socket      =  config.socket;
        console.log("Socket:",this.socket);
        this.sock        = new ipc(this);
        console.log(this.friendly_name, "Connect to Socket");
       // console.log("Sock:",this.sock);
        
        this.sock.connect(this);
        this.mode        = null;
        this.timeout     = 5000;
        this.xbeeAddress = this.sock.name;
        this.urn = "urn:dev:xbee:" + this.xbeeAddress +":nid:" + fas.nodeId2EntityId(this.id);
        
        if (config.hasOwnProperty("latlon") && config.latlon.split(",").length == 2) {
            this.latitude = config.latlon.split(",")[0];
            this.longitude = config.latlon.split(",")[1];
        }
	
        this.cache = {};
        // Endpoint must be overriden in child node classes
        this.fapiEndpoint = "";
        
        console.log("Adding event handler: ",this.urn);
        this.on(this.urn,(record) => {
           console.log(this.friendly_name,"Processing record",record);
           this.handleUpdate(record);
        });


        setTimeout(() =>{this.lastResponse();},2000);
        
        // Set up event listeners
        this.on("request", () => {
        if (this.mode != "connected"){
               //this.lastResponse();
               return;
            }
            console.log(this.friendly_name,"got a request to update");
            this.updateState();
        });

        this.on("input", (data) => {
            console.log("on input",data);
           // if (this.mode != "connected") {
               // if we are waiting on radio, show the lastResponse
               //this.lastResponse(data.payload);
             //  return;
            //}
            console.log('node data', data.payload);
            this.handleNodeInput(data.payload);
        });

        this.on("connected", () => {
            this.mode = "connected";
            console.log(this.friendly_name, "on connected");
            this.setStatusConnected();
        });

        this.on("disconnected", () => {
            this.mode = null;
            this.setStatusDisconnected();
        });

        this.on("busy", (state) => {
            console.log("busy", state, this.mode);
            if (!state && this.mode == "busy") {
                this.mode = "connected";
                this.setStatusConnected();
                return;
            }

            if (this.mode == "updating")
                return;

            if (!state) return;
            console.log("setBusy");
            this.mode = "busy";
            this.setStatusBusy(state);
        });

        this.on("data", (data) => {
            console.log("on data",this.friendly_name,data);
            if (!data.length)
                return;
            try {
                this.handleResponse(data);
            } catch (e) {
                console.error(e);
            } finally {;
            }
        });

        // Update node immediatly
        // Socket is not availible immedidatly, so add delay. 
        // Later, might implement queue in IPCsocket so this will no longer be neccisacary
        //setTimeout(this.updateState.bind(this), 1500);
    }

    queueRequest(request,attempts=0) {
        console.log("Queued request", request, this.mode);
        if (this._qTimeout) clearTimeout(this._qTimeout);
        if (this.mode != "connected" ) {
            this._qTimeout = setTimeout(() => {
                if (attempts < MAX_QUEUED_ATTEMPTS)
                    this.queueRequest(request,attempts+1);
                else
                    console.log("Max attempts reached - abandon request");
            }, 1000);
            return;
        } else {
            console.log("Updating state for:", this.port);
            console.log("Sending to end node: ", request);
            this.setUpdating();
            this.sock.setBusy(true);
            this.sendRequest(request);
        }
    }

    updateState() {
        var request = this.createRequest();
        this.queueRequest(request);
    }

    lastResponse() {
        if ( fas.getDB() ){
            try{
                fas.getDB().get(this.urn,(err,lastUpdate) =>{
                if (err)
                     return;
                this.handleResponse(new Buffer(lastUpdate,'binary'),false);
             });
            } catch(e){
                console.log("DB Error");
            }
        }
    }

    // Main update routine. Liam recommends you override called methods instead of this big fatty

    handleResponse(updateData,updateDB =true) {
        // Decode recevided data
        var senML;
        var self = this;
        try {
            senML = this.decodeUpdate(updateData);
            //console.log(senML);
            if (!api.parseSenML(senML)) {
                throw new Error("Received data is not valid SenML");
            }
            senML.forEach((r)=>{
                console.log(r.nid());
                self.emit(r.urn(),r);
            });
	    
            if (updateDB && this.urn == senML.urn() && fas.getDB()){
                 fas.getDB().put(this.urn,updateData.toString('binary'));
            }
        } catch (e) {
            console.error(e);
//            this.sendToNodeOut("Failed to decode data from socket");
            this.setStatusConnected();
            console.error(e);
            return;
        }
    }

    handleError(record){
        this.sendToNodeOut(record.valueString());
        throw new Error(record.valueString());
        return null;
    }
    
    handleUpdate(record) {

        // Extract Data from Record
        var sensorReadings;
        try {
            console.log("sensorReadings");
            sensorReadings = this.extractReadings(record);
            if (!sensorReadings)
                return false;
            console.log("Got", sensorReadings);

        } catch (e) {
            console.error("Failed to extract data from record");
            console.error(e);
            return;
        }

        var msg = {
            topic: this.friendly_name,
            payload: sensorReadings.state
        };

        // Update Fass
        try {
            console.log("updatingFASS");
            this.updateFASS(sensorReadings);
            this.sendToNodeOut(JSON.stringify(record));
	    
            //this.send(sensorReadings); TODO: Implement when third party connections
        } catch (e) {
            console.error("Failed to update FASS");
            console.error(e);
        } finally {
            // Updating is done, make sure the node's status is "connected"
            console.log(`Setting $ {this.friendly_name} to connected...`);
            this.mode = "connected";
            this.setStatusConnected();
        	   this.sock.setBusy(false);
        }
    }

    // Decode messagePack
    decodeUpdate(data) {
        var d = msp.decode(data);
        console.log("Received senML:", JSON.stringify(d));
        return msp.decode(data);
    }

    // Throws an error if received SenML is invalid.
    validateSenML(senML) {
        if (typeof senML !== "object") {
            throw new Error(`Decoded data is type: $ {
                typeof senML
            },
            expected object`);
        }
        if (!api.parseSenML(senML)) {
            throw new Error("Received data is not valid SenML");
        }
    }


    // Process a record containing relevant information
    extractReadings(record) {
        console.log("Extracting data from record: ", record);
        var data = {
            state: record.value()
        };
        // Add postition data if the record contains it

        if (record.pos()) {
            //var coords = senML[0].pos;
            data.gps = record.pos();
        }
        return data;
    }
    
    

    // Send new readings to FASS
    updateFASS(body) {
        // Populate body object with stuff
        body.id = fas.nodeId2EntityId(this.id);
        if (this.friendly_name) {
            body.friendly_name = this.friendly_name;
        }

        // Check update URL is defined
        if (!this.fapiEndpoint) {
            throw new Error("Update URL is empty. Be sure to override it in your Node Class!");
            return;
        }

        // Encode body and ship it off to farm-assistant
        var jsonBody = JSON.stringify(body);
        console.log("updating fass with JSON:", jsonBody);

        var request = fas.clientRequest({
            path: this.fapiEndpoint,
            body: jsonBody
        });
        request.write(jsonBody);
        request.end();
    }


    // Show "connected" status in NodeRED
    setStatusConnected() {
        clearTimeout(this._qConnectTimeout);
        console.log("setConnected", this.mode);
        var connectedStatus = {
            fill: "green",
            shape: "dot",
            text: "connected"
        };
        this.status(connectedStatus);
    }

    // Show "updating" status in NodeRED
    setUpdating() {
        this.mode = "updating";
        this.setStatusUpdating();
        clearTimeout(this._qConnectTimeout );
        this._qConnectTimeout = setTimeout(() => {
           if (this.mode == "updating"){
                console.log("Resetting in setUpdating");
                this.sock.reset();
           }
        }, MIN_WAIT_UPDATING);
    }
    
    setStatusUpdating() {
        var updatingStatus = {
            fill: "yellow",
            shape: "dot",
            text: "updating"
        };

        this.status(updatingStatus);
    }

    // Show "updating" status in NodeRED
    setStatusBusy() {
        var busyStatus = {
            fill: "yellow",
            shape: "dot",
            text: "busy"
        };

        this.status(busyStatus);
    }

    // Show "disconnected" status in NodeRED
    setStatusDisconnected() {
        var disconnectedStatus = {
            fill: "red",
            shape: "ring",
            text: "disconnected"
        };

        this.status(disconnectedStatus);
    }

    // Is the socket connected?
    isSocketConnected() {
        //return this.sock && this.sock.socketSingleton.connected;
        return this.sock; // && this.sock.connected;
    }


    // Send a senML request to the socket
    sendRequest(senML) {
        if (!this.isSocketConnected()) {
            console.log("Sending Request not sent because socket is not connected");
            return;
        }
        if (this.busy) {
            console.log("Request not sent because socket busy");
            return;
        }

        var buff;

        try {
            buff = msp.encode(senML);
        } catch (e) {
            console.error("Failed to encode data to MessagePack");
            return;
        }

        //this.sock.socketSingleton.write(buff);
        this.sock.write(buff);
    };



    // Create update request to send to the sensors
    createRequest(data =null) {
        throw new Error("makeRequest is not implemented by subclass");
    }

    // Handle input from Node connection
    handleNodeInput(data) {
        // Listen to update requests
        if (data.hasOwnProperty("payload")){
            data = data.payload;
        }
        var request  = this.createRequest(data);
        console.log("handlNodeInput - Sending:",data,"=>",request);
        this.queueRequest(request);
    }

    // Send data to NodeRed Node output
    sendToNodeOut(msg) {
        this.send({"payload":msg});
    }
}

module.exports = AgsenseNode;
