const xst = require('fa-gateway-lib');
const config = require('farm-assistant-shim/config');
const logger = require('farm-assistant-shim/config').logger;
const events = require("../../red/runtime/events");
const socketsPath = config.gateway.socketsPath;
var  faConfig    = config;

"use strict";
var util = require("util");
var isUtf8 = require('is-utf8');
var EventEmitter = require("events");
var net = require("net");
var fs = require("fs");
var path = require("path");

var Events = EventEmitter.EventEmitter;

var Sockets={};
var owners   = []; 
var sockets = [];

function global_reset(){
	Sockets={};
	owners   = []; 
	sockets = [];
} 

// "Private" Socket object
class IPCSocket extends EventEmitter {

    constructor(name) {
        super();
        var $this = this;
        this.name = name;
        this.socketPath = IPCSocket._formatSocketPath(name);
        this.connected = false;
        this.sock = null;
        this._busy = false;
	this._icheck = 0;
        
        this.reset  =function(){
            console.log("IPC Socket","reset");
            $this.sock.emit("end");
        }

        this.destroy  =function(){
            console.log("IPC Socket","destroy");
            $this.sock.emit("end");
            clearTimeout(this._icheck);
        }

        this.setBusy = function(state = true) {
            $this._busy = state;
            console.log("IPC Socket", "busy", state);
            $this.emit('busy', state);
        }

        this.receivingTimeout = 500;
        // Buffer to hold multi-packet incoming transmissions
        this.receivingBuffer = Buffer(0);

        this._icheck = setInterval(() => {
            if (!this.connected) 
		this.emitData();
        }, 2000);
    }

    // Return a fresh socket
    _createSocket() {
        return net.createConnection({
            path: this.socketPath,
            timeout: this.receivingTimeout
        });
    }

    // Create the absolute filename for the unix socket 
    static _formatSocketPath(name) {
        return path.format({
            dir: socketsPath,
            base: name,
            ext: ''
        });
    }

    // Write data to the socket
    write(data) {
        console.log("Writing ", data,this.socketPath);
	
        if (this.connected) {
            this.sock.setTimeout(100);
            this.sock.write(data);
        }
    }


    // Callback when socket has connected, callback reject() on error.
    connect(success, reject) {
        var sock = this._createSocket();
        sock.on('connect', () => {
            success(sock);
        });
        sock.on('error', (error) => {
            reject(error);
        });
    }

    // Emit data received from IPC Socket, reconnects on error
    emitData() {
        // Store reference to object instance
        var self = this;
        if (!_socketExists()) {
	    console.log("Socket not exists");
            return _handleDisconnect();
        }
        this.connect((sock) => {
            console.log("IPC Node", "Connect");
            self.sock = sock;
            self.sock.on('data', _processData);
            self.sock.on('error', _handleDisconnect);
            self.sock.on('timeout', _flushData);
            self.sock.on('end', _handleDisconnect);
	    self.sock.setTimeout(100);
            _setConnected();
            //self.sock.on('data', (data) =>_processData(data));
            //self.sock.on('error', (error) => _handleDisconnect(error));
            //self.sock.on('timeout', () => _flushData());
            // self.sock.on('end', () => _handleDisconnect());
        }, (error) => {
            _handleDisconnect(error);
        });

        function _socketExists() {
            try {
                return fs.existsSync(self.socketPath);
            } catch (e) {
                return false;
            }
        }

        function _setConnected() {
            self.emit('connected');
            self.connected = true;
        }

        function _setDisconnected() {
            self.emit('disconnected');
            self.connected = false;
        }

        // Meat and Potatos of the whole class


        function _processData(data) {
            console.log("IPC","Got data");
            self.receivingBuffer = Buffer.concat([self.receivingBuffer, data]);
        }

        function _flushData() {
	    console.log("Flushing", self.receivingBuffer.length);
            if (self.receivingBuffer.length) {
                console.log("IPC Flushing", self.receivingBuffer);
                self.emit('data', self.receivingBuffer);
                self.receivingBuffer = Buffer(0);
            }
        }

        // Handle errors


        function _handleDisconnect(error = null) {
            console.log("IPC Node", "DisConnect");
            // If error is null, treat disconnect as friendly   
            if (error) {
                console.error(error);
            }
            if (self.sock != null) {
                console.log("IPC Socket - handle disconnect");
                self.sock.removeListener('data', _processData);
                self.sock.removeListener('error', _handleDisconnect);
                self.sock.removeListener('timeout', _flushData);
                self.sock.removeListener('end', _handleDisconnect);
                self.sock.destroy();
            }
            _setDisconnected();
        }
    }
}




class IPCSocketSingleton {

    constructor() {
    }

    getInstance(socketName) {
        console.log("IPCSocketSingleton", socketName);

        if (Sockets.hasOwnProperty(socketName)) {
	    console.log("Usung existing socket for ",socketName);
            return Sockets[socketName];
        } else {
	    console.log("Creating new socket for ",socketName);
            var new_sock = new IPCSocket(socketName);
            Sockets[socketName] = new_sock;
            return new_sock;
        }
    }
}

function IPCSocketNode(n) {
    var node     = this;
    node.socket  = n.socket;
    
    // Configuration options passed by Node Red
    var _data = function(data) {
        owners.forEach(function(owner) {
            owner.emit('data', data);
        });
    };

    var _error = function(error) {
        console.log("Handling error!", error);
        owners.forEach(function(owner) {
            owner.emit('error', error);
        });
    };

    var _busy = function(state) {
        owners.forEach(function(owner) {
            owner.emit('busy', state);
        });
    };

    var _connected = function() {
        owners.forEach(function(owner) {
            owner.emit('connected');
        });
    };

    var _disconnected = function() {
        owners.forEach(function(owner) {
            owner.emit('disconnected');
        });
    };

    //console.log("this.socket is ", node.socket);

    node.connect = function(owner) {
        console.log("Starting IPC", this.id, owner.friendly_name);
        node.socketSingleton = new IPCSocketSingleton().getInstance(node.socket);
        node.name = node.socketSingleton.name;
        node.write = node.socketSingleton.write.bind(node.socketSingleton);
        node.reset = node.socketSingleton.reset.bind(node.socketSingleton);

        node.setBusy = node.socketSingleton.setBusy;

	if (sockets.indexOf(node.socketSingleton) == -1) {
            console.log("IPC Setting up event handlers!");
	    sockets.push(node.socketSingleton);
            node.socketSingleton.on('busy', _busy);
            node.socketSingleton.on('data', _data);
            node.socketSingleton.on('error', _error);
            node.socketSingleton.on('connected', _connected);
            node.socketSingleton.on('disconnected', _disconnected);
	    owner.on('close',()=>{
		console.log("Closing Socket");
                node.socketSingleton.removeListener('busy', _busy);
                node.socketSingleton.removeListener('data', _data);
                node.socketSingleton.removeListener('error', _error);
                node.socketSingleton.removeListener('connected', _connected);
                node.socketSingleton.removeListener('disconnected', _disconnected);
                node.socketSingleton.destroy();
	 	var idx = sockets.indexOf(node.socketSingleton);
		if (idx !=-1)
			sockets.splice(idx,1);
		});
        }
        owners.push(owner);
	owner.on('close',()=>{
	 	var idx = owners.indexOf(owner);
		console.log("Closing Node",idx);
		if (idx !=-1)
			owners.splice(idx,1);
		console.log("Owners Remaining",owners.length);
		if (owners.length == 0)
			global_reset();
			
	});
    }
}

module.exports = IPCSocketNode;
