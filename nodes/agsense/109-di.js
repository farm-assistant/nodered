const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();

var AgsenseNode = require("./base");

module.exports = function(RED) {

    class DiNode extends AgsenseNode {

        setFApiEndpointByKnd(knd) {
            this.knd = 'TANKSENSOR';
            if (['TANKSENSOR', 'FLOWSENSOR', 'ELECTRICFENCE', 'DISENSOR'].indexOf(knd) !== -1) {
                this.knd = knd;
            }

            var fapiEndpoint = null;
            switch (this.knd) {
                case 'FLOWSENSOR':
                    fapiEndpoint = "/api/services/flowsensor/state";
                    break;
                case 'FENCESENSOR':
                    fapiEndpoint = "/api/services/fencesensor/state";
                    break;
                case 'DISENSOR':
                    fapiEndpoint = "/api/services/disensor/state";
                    break;
                case 'TANKSENSOR':
                default:
                    fapiEndpoint = "/api/services/tanksensor/state";
                    break;
            }
            this.fapiEndpoint = fapiEndpoint;
        }

        constructor(config) {
            super(RED, config);

            this.port = config.pin;
            // state is boolean or null if undetermined
            this.state = null;
            // represents the requested state, if different from this.state, then node has requested state change in sensor;
            this.requestedState = null;
            this.setFApiEndpointByKnd(config.knd);
        }

        // Return human string for current state of node
        getStateString() {
            console.log("DI this.state:", this.state, this.state ? "on" : "off");
            if (this.state === null) {
                return "undetermined";
            } else {
                return this.state ? "on" : "off";
            }
        }

        // Return human string for the requested state
        getRequestedStateString() {
            if (this.requestedState === null) {
                return "undetermined";
            } else {
                return this.requestedState ? "on" : "off";
            }
        }

        setStatusConnected() {
            console.log(`DI [${this.knd}]: turning connected`);
            var connectedStatus = {
                fill: "green",
                shape: "dot",
                text: "connected"
            };

            connectedStatus.text = "connected: " + this.getStateString();

            this.status(connectedStatus);
        }


        // Display updating status with target state
        setStatusUpdating() {
            console.log("DI: updating");
            var updatingStatus = {
                fill: "yellow",
                shape: "dot",
                text: "updating"
            };

            if (this.requestedState !== null && this.state !== this.requestedState) {
                updatingStatus.text = "updating: turning " + this.getRequestedStateString();
            }
            this.status(updatingStatus);
        }



        createRequest(data = null) {
            var new_state = {
                n: this.port,
                vi: { knd: this.knd }
            };
            


	    if (data && "vb" in data)
        		  new_state.vb = (data.vb === true);
	    else
        		  new_state.vb = this.requestedState;

            var senML = api.makeSenML({
                bn: this.urn,
            }, [new_state]);
            return senML;
        }


        extractReadings(record) {
            console.log("extractReadings", record);
            this.state = record.valueBoolean();
            super.extractReadings(record);

            var data = {
                state: record.valueBoolean() ? "on" : "off"
            };
            return data;
        }
    }


    RED.nodes.registerType("di", DiNode);
}
