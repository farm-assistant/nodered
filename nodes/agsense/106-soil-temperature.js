"use strict"
const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();
const msp = require("msgpack5")();

var AnalogNode = require("./analog");

module.exports = function (RED) {

        class TemperatureNode extends AnalogNode {
                constructor(config) {
                        super(RED, config);
                        this.fapiEndpoint = "/api/services/soil_temperature/state";
                }


                processValue(value) {
                        return Number(value.toFixed(2));
                }
        }

        RED.nodes.registerType("soil-temperature", TemperatureNode);
}

