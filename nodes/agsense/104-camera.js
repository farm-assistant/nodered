const api = require("fa-gateway-lib").api();
const fs = require("fs");

var AgsenseBase = require("./base.js");

module.exports = function(RED) {

		class CameraNode extends AgsenseBase {
				constructor(config) {
						super(RED, config);   
      this.timeout = 5 * 60000;
      this.port = "CAM";
						this.irc = eval(config.irc);
						this.res = eval(config.res);
						this.exp = eval(config.exp);
						console.log("Node Camera",this.irc,this.res,this.exp);
						this.fapiEndpoint = "/api/services/camera/state";
				}

				createRequest() {
						return api.makeSenML(
								{bn: this.urn },
								[{
									n: this.port,
								 	vi : {exp:this.exp,res:this.res,irc:this.irc}
								}]
						);
				}


				// Extract camera data and convert to JPEG stream
				extractReadings(record) {
						var body = {};
						console.log("camera readings extractor ",record.valueBinary().length);
						/*fs.writeFile("/data/images/test.jpg", record.valueBinary(),  "binary",function(err) {
   						 if(err) {
       								 console.log(err);
   						 } else {
        					console.log("The file was saved!");
    						  }
						});*/
        
						body.frame = {
								timestamp: new Date().getTime(),
								data: record.valueBinary().toString('base64'),
						}
						return body;
				}
		}
		RED.nodes.registerType("camera", CameraNode);
}

