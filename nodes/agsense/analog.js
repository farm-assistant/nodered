"use strict"

const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();
const msp = require("msgpack5")();

var AgsenseNode = require("./base");

class AnalogNode extends AgsenseNode {
        constructor(RED, config)  {
                super(RED, config);
                this.port = config.port;
        }

        // Create a SenML Update Request 
        createRequest() {
				console.log("Creating request with port:", this.port);
                return api.makeSenML(
                        {bn: this.urn },
                        [{
                                n: this.port,
                                v: null
                        }]
                );
        }


        // Extract analog reading from record. 
        extractReadings(record) {
                var data = {
                        state: this.processValue(record.value())
                };

                if (record.pos()) {
                        data.gps =record.pos(); 
                }

                return data;
        }

        // Convert value in senML to value in fapi update body
        processValue(val) {
                return parseFloat(val);
        }
}

module.exports = AnalogNode;

