"use strict"
const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();
const msp = require("msgpack5")();

var AnalogNode = require("./analog");

module.exports = function (RED) {

        class MoistureNode extends AnalogNode {
                constructor(config) {
                        super(RED, config);
                        this.fapiEndpoint = "/api/services/soil_moisture/state";
                }


                processValue(value) {
			return Number(value.toFixed(2));
                }
        }

        RED.nodes.registerType("soil-moisture", MoistureNode);
}

