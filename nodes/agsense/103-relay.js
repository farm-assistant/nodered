const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();

var AgsenseNode = require("./base");

module.exports = function(RED) {
    RED.httpAdmin.post("/devices/relay/turn_on", function(req, res) {
        try {
            console.log("turning on", req.body.id);
            var n = fas.entityId2Node(req.body.id);
            n.turnOn();
            return res.status(200).json({
                status: "OK"
            });
        } catch (e) {
            console.error("tunring on failed:", e);
            return res.status(500).json({
                status: "ERROR"
            });
        }
    });

    RED.httpAdmin.post("/devices/relay/turn_off", function(req, res) {
        try {
            console.log("turning off", req.body.id);
            var n = fas.entityId2Node(req.body.id);
            n.turnOff();
            return res.status(200).json({
                status: "OK"
            });
        } catch (e) {
            console.error("tunring off failed:", e);
            return res.status(500).json({
                status: "ERROR"
            });
        }
    });


    class RelayNode extends AgsenseNode {
        constructor(config) {
            super(RED, config);

            this.port = config.pin;
            // state is boolean or null if undetermined
            this.state = null;
            // represents the requested state, if different from this.state, then node has requested state change in sensor;
            this.requestedState = null;
            this.fapiEndpoint = "/api/services/relay/state";

        }

        // Return human string for current state of node
        getStateString() {
            console.log("Relay this.state:", this.state, this.state ? "on" : "off");
            if (this.state === null) {
                return "undetermined";
            } else {
                return this.state ? "on" : "off";
            }
        }

        // Return human string for the requested state
        getRequestedStateString() {
            if (this.requestedState === null) {
                return "undetermined";
            } else {
                return this.requestedState ? "on" : "off";
            }
        }

        setStatusConnected() {
            console.log("Relay: turning connected");
            var connectedStatus = {
                fill: "green",
                shape: "dot",
                text: "connected"
            };

            connectedStatus.text = "connected: " + this.getStateString();

            this.status(connectedStatus);
        }


        // Display updating status with target state
        setStatusUpdating() {
            console.log("Relay: turning updating");
            var updatingStatus = {
                fill: "yellow",
                shape: "dot",
                text: "updating"
            };

            if (this.requestedState !== null && this.state !== this.requestedState) {
                updatingStatus.text = "updating: turning " + this.getRequestedStateString();
            }
            this.status(updatingStatus);
        }

        turnOn() {
            console.log("turning on");
            this.requestedState = true;
            //this.setUpdating();
            var turnOnRequest = this.createRequest();
            this.queueRequest(turnOnRequest);
        }

        turnOff() {
            console.log("turning off");
            this.requestedState = false;
            //this.setUpdating();
            var turnOffRequest = this.createRequest();
            this.queueRequest(turnOffRequest);
        }


        createRequest(data = {}) {
            var new_state = {
                n: this.port
            };

            if (typeof data === 'object' && data.hasOwnProperty("vb"))
                 new_state.vb = (data.vb === true || parseInt(data.vb) === 1);
            else
                 new_state.vb = this.requestedState;

            var senML = api.makeSenML({
                bn: this.urn,
            }, [new_state]);
            return senML;
        }


        // Ensure this is NOT a SET request 
        lastResponse(data={}) {
            if (!data.hasOwnProperty("vb") || data.vb == undefined)
                super.lastResponse();
        }
         
        extractReadings(record) {
            console.log("extractReadings", record);
            this.state = record.valueBoolean();
            super.extractReadings(record);

            var data = {
                state: record.valueBoolean() ? "on" : "off"
            };

            return data;
        }


    }


    RED.nodes.registerType("relay", RelayNode);
}
