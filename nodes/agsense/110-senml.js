const fas = require("farm-assistant-shim");
const api = require("fa-gateway-lib").api();

var AgsenseNode = require("./base");

module.exports = function(RED) {
    class SenMLNode extends AgsenseNode {
        constructor(config) {
            super(RED, config);
            this.senml = config.senml;
            this.friendly_name = config.friendly_name;
            this._senml = JSON.parse(this.senml.toString());
            this.port = this._senml.n;
            var node = this;
            console.log("SenML String", this.senml,"ID",this.id);
        }

        createRequest(data ={}) {

            var senML = api.makeSenML({
                bn: this.urn,
            }, [this._senml]);

        	   senML[1] = Object.assign(data,senML[1]);

            console.log("SenML Object", senML);
            return senML;
        }
        
        extractReadings(record) {
            if ( record.name() == "error" ) {
                super.handleError(record);
                return null;
            }

            this.sendToNodeOut({topic:record.n,payload:record});
            return super.extractReadings(record);
        }


        updateFASS(request) {
            return true;
        }
    }


    RED.nodes.registerType("senml", SenMLNode);
}
