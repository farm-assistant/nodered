'use strict';
const _ = require('lodash');
const querystring = require('querystring');
const fas = require('farm-assistant-shim');
const faConfig = require('farm-assistant-shim/config');
const http = require('http');

module.exports = (RED) => {
  function UpdatesNode(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    let updatedAt = new Date().toISOString();

    const call = () => {
      const calledAt = new Date().toISOString();
      const query = {
        start: updatedAt,
        end: calledAt,
      };

      const request = http.request({
        hostname: faConfig.http.host,
        port: faConfig.http.port,
        path: `/api/history/period?${querystring.stringify(query)}`,
        method: 'GET',
        headers: {
          'x-ha-access': faConfig.password,
        },
      }, (response) => {
        let body = '';
        response.on('data', chunk => body += chunk);
        response.on('end', () => {
          updatedAt = calledAt;
          process(JSON.parse(body));
          setTimeout(call, 10 * 1000);
        });
      });

      request.end();
    };

    const process = (entities) => {
      _.forEach(entities, (entity, entityId) =>
        _.forEach(entity.states, state =>
          node.send({
            payload: {
              entity_id: entityId,
              friendly_name: entity.friendly_name,
              latitude: entity.latitude,
              longitude: entity.longitude,
              value: state.value,
              updated_at: state.updated_at,
            },
          })));
    };

    call();
  }

  RED.nodes.registerType('updates', UpdatesNode);
};
